'use strict';
const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

function resolve(dir) {
  return path.join(__dirname, '..', dir);
}

const isProd = process.env.NODE_ENV === 'production';
const extOut = process.env.TARGET === 'extension';

const webpackConfig = {
  mode: isProd ? 'production' : 'development',
  context: path.resolve(__dirname, '../'),
  entry: {
    index: './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, extOut ? '../ext/dist' : '../dist'),
    publicPath: './',
    filename: '[name].js',
    chunkFilename: '[id].js'
  },
  devtool: isProd ? 'cheap-module-source-map' : 'eval',
  resolve: {
    extensions: ['.js', '.json', '.svg'],
    alias: {
      '@': resolve('src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          resolve('src')
          // TODO: setup hot module replacement
          // resolve('node_modules/webpack-dev-server/client')
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: '[name].[hash:7].[ext]'
        }
      }
    ]
  },
  plugins: [new CleanWebpackPlugin()]
};

module.exports = webpackConfig;
