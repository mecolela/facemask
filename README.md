# [Face Mask](https://gitlab.com/mecolela/facemask)
[![pipeline status](https://gitlab.com/mecolela/facemask/badges/master/pipeline.svg)](https://gitlab.com/mecolela/facemask/commits/master)
> An Experimental Project

Mask / filter your face while on a video call / browsing the web with a virtual camera.

### Setting up

```bash
# install dependencies
yarn install

# build source
yarn build
```

### Development & testing

Project is in 2 parts. A chrome extension and a simple HTML page for debugging purposes. 

#### Chrome Extension

1. Enable developer mode in chrome (the extensions page).
2. Load an unpacked extensions, located in the ``ext`` directory
3. Open up Google Meet
4. Change to the ``Face Mask`` camera as your video input.
5. Try join a call (still unstable)

> Models may take a while to load if they aren't cached

#### Debugging Page

Run in this directory
```bash
yarn serve
```

Allow the site opened to access your camera
