export function linearInterpolate(v0, v1, perc) {
  return v0 + (v1 - v0) * perc;
}

export function random(v0, v1) {
  return v0 + Math.random() * (v1 - v0);
}

/*
* Generate a transform function of p in the coordinate system defined by p0 and p1.
*/
export function getTransformFunc(p0, p1, p) {
  let d = p1.subtract(p0);
  let dir = d.normalize();
  let l0 = d.length;
  let n = dir.clone();
  n.angle += 90;
  let v = p.subtract(p0);
  let x = v.dot(dir);
  let y = v.dot(n);
  return (p0New, p1New) => {
    let d = p1New.subtract(p0New);
    if (d.length === 0) {
      return p0New.clone();
    }
    let scale = d.length / l0;
    let dirNew = d.normalize();
    let nNew = dirNew.clone();
    nNew.angle += 90;
    return p0New.add(dirNew.multiply(x * scale)).add(nNew.multiply(y * scale));
  };
}

export function getClosestPointOnSegment(p0, p1, p) {
  let d = p1.subtract(p0);
  let c = p.subtract(p0).dot(d) / (d.dot(d));
  if (c >= 1) {
    return p1.clone();
  } else if (c <= 0) {
    return p0.clone();
  } else {
    return p0.add(d.multiply(c));
  }
}

/* Check if v0 and v1 are collinear.
* Returns true if cosine of the angle between v0 and v1 is within threshold to 1.
* */
export function isCollinear(v0, v1, threshold = 0.01) {
  let colinear = false;

  if (v0 && v1) {
    let n0 = v0.normalize();
    let n1 = v1.normalize();
    colinear = Math.abs(n0.dot(n1)) > 1 - threshold;
  }

  return colinear;
}

export function gaussian(mean, variance) {
  var u = 0, v = 0;
  //Converting [0,1) to (0,1)
  while (u === 0) u = Math.random();
  while (v === 0) v = Math.random();
  let value = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
  return value * variance + mean;
}

export function clamp(v, minV, maxV) {
  return Math.min(Math.max(v, minV), maxV);
}
