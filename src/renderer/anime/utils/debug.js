import { getAdjacentKeyPoints } from '@tensorflow-models/posenet';

const lineWidth = 2;

export function drawPoint(ctx, y, x, r, color = 'red') {
  ctx.beginPath();
  ctx.arc(x, y, r, 0, 2 * Math.PI);
  ctx.fillStyle = color;
  ctx.fill();
}

/**
 * Draws a line on a canvas, i.e. a joint
 */
export function drawSegment([ay, ax], [by, bx], color, scale, ctx) {
  ctx.beginPath();
  ctx.moveTo(ax * scale, ay * scale);
  ctx.lineTo(bx * scale, by * scale);
  ctx.lineWidth = lineWidth;
  ctx.strokeStyle = color;
  ctx.stroke();
}

function toTuple({y, x}) {
  return [y, x];
}

/**
 * Draws a pose skeleton by looking up all adjacent key points/joints
 */
export function drawSkeleton(kps, minConfidence, ctx, scale = 1, color = 'cyan') {
  const adjacentKeyPoints = getAdjacentKeyPoints(kps, minConfidence);

  adjacentKeyPoints.forEach((kp) => {
    drawSegment(toTuple(kp[0].position), toTuple(kp[1].position), color, scale, ctx);
  });
}

/**
 * Draw pose key points onto a canvas
 */
export function drawKp(kps, minConfidence, ctx, scale = 1, color = 'aqua') {
  for (let i = 0; i < kps.length; i++) {
    const kp = kps[i];

    if (kp.score < minConfidence) {
      continue;
    }

    const {y, x} = kp.position;
    drawPoint(ctx, y * scale, x * scale, 3, color);
  }
}

/**
 * For loop abstraction for improved performance
 * array {Array}
 * func {Function}
 */
export function loop(array, func) {
  for (let ix = 0; ix < array.length; ix++) {
    func(array[ix], ix)
  }
}
