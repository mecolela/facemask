import paper from 'paper';

export function importFile(file) {
  const scope = new paper.PaperScope();
  const canvas = scope.createCanvas(0, 0);
  scope.setup(canvas);

  return new Promise((res, rej) => {
    const src = `${window.FMBU}/${file}`;
    scope.project.importSVG(src, () => {
      res(scope);
    }, (e) => {
      console.error('Import Error', e);
      rej(e);
    });
  });
}

export function isPath(item) {
  return item.constructor === item.project._scope.Path;
}

export function isShape(item) {
  return item.constructor === item.project._scope.Shape;
}

export function isGroup(item) {
  return item.constructor === item.project._scope.Group;
}

export function findFirstItemWithPrefix(root, prefix) {
  let items = root.getItems({recursive: true});
  for (let i = 0; i < items.length; i++) {
    if (items[i].name && items[i].name.startsWith(prefix)) {
      return items[i];
    }
  }
  return null;
}
