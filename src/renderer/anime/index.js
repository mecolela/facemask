import { browser as tfB } from '@tensorflow/tfjs';
import { load as poseNetLoader } from '@tensorflow-models/posenet';
import { load as faceMeshLoader } from '@tensorflow-models/facemesh';

import { debug } from './utils';
import { facePartToIndex } from './skeleton/part';

/**
 * Animation Renderer
 */
class AnimeRenderer {
  constructor(video, canvas, opts = {}) {
    this.video = video;

    // Canvas holding input frames from the stream
    this.videoCvs = document.createElement('canvas');
    this.videoCtx = this.videoCvs.getContext('2d');
    this.videoCvs.width = video.videoWidth;
    this.videoCvs.height = video.videoHeight;

    console.debug('Input dimensions:', `${video.videoWidth}X${video.videoHeight}`);

    // MODELS
    this.posenet = null;
    this.facemesh = null;

    // CONFIG Elements
    this.DEBUG = opts.debug || true;
    this.poseMaxDetection = opts.poseMaxDetection || 1;
    this.poseScoreThreshold = opts.poseScoreThreshold || 0.1;
    this.partScoreConfidence = opts.poseScoreThreshold || 0.15;

    // OUTPUT Element
    this.outKpCvs = canvas;
    this.outKpCtx = canvas.getContext('2d');
    this.outKpCvs.width = video.videoWidth;
    this.outKpCvs.height = video.videoHeight;

  }

  /**
   * Asynchronously initializes the renderer
   */
  async init() {
    await Promise.all([
      // Loading in the models
      this._loadModels(),
      // Prepare the character
      this._loadSVG()
    ]);
  }

  /**
   * Loads in the required models
   * @private
   */
  async _loadModels() {
    console.debug('Loading in models');
    const pL = poseNetLoader({
      architecture: 'MobileNetV1',
      quantBytes: 2,
      multiplier: 0.5,
      outputStride: 16,
      inputResolution: 200
    });
    const fL = faceMeshLoader({maxFaces: 1});

    const [posenet, facemesh] = await Promise.all([pL, fL]);

    console.debug('Models loaded');
    this.posenet = posenet;
    this.facemesh = facemesh;
  }

  /**
   * Loads in the selected SVG mask / resource
   * @private
   */
  async _loadSVG() {
    // this.scope = await svg.importFile(BoySVG, this.canvas);
    // console.debug('Skinned skeleton loaded');
  }

  /**
   * Detects a pose from the input stream & passes the result to the output stream
   * @private
   */
  async _detectPose() {
    const vidW = this.video.videoWidth;
    const vidH = this.video.videoHeight;
    this.videoCtx.clearRect(0, 0, vidW, vidH);
    // Draw video
    this.videoCtx.save();
    this.videoCtx.scale(-1, 1);
    this.videoCtx.translate(-vidW, 0);
    this.videoCtx.drawImage(this.video, 0, 0, vidW, vidH);
    this.videoCtx.restore();

    // Creates a tensor from an image
    const input = tfB.fromPixels(this.videoCvs);
    const faceP = this.facemesh.estimateFaces(input, false, true);
    const poseP = this.posenet.estimatePoses(input, {
      nmsRadius: 30.0,
      flipHorizontal: true,
      decodingMethod: 'multi-person',
      maxDetections: this.poseMaxDetection,
      scoreThreshold: this.poseScoreThreshold
    });

    const [facePred, posePred] = await Promise.all([faceP, poseP]);
    input.dispose();

    this.outKpCtx.clearRect(0, 0, vidW, vidH);

    // if (this.DEBUG) {
    debug.loop(posePred, ({score, keypoints}) => {
      if (score >= this.partScoreConfidence) {
        debug.drawKp(keypoints, this.poseScoreThreshold, this.outKpCtx);
        debug.drawSkeleton(keypoints, this.poseScoreThreshold, this.outKpCtx);
      }
    });

    debug.loop(facePred, face => {
      const indices = Object.values(facePartToIndex);
      debug.loop(indices, (index) => {
        const p = face.scaledMesh[index];
        debug.drawPoint(this.outKpCtx, p[1], p[0], 2);
      });
    });
    // }
  }

  /**
   * Set the canvas & it's contexts dimensions
   * @private
   */
  setSize(w, h) {
    // if (this.DEBUG) {
    this.videoCvs.width = w;
    this.videoCvs.height = h;

    this.outKpCvs.width = w;
    this.outKpCvs.height = h;
    // }
  }

  /**
   * Executes the face + pose detection
   */
  render(t) {
    this._detectPose();
  }
}

export { AnimeRenderer };
