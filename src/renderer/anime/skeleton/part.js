import { svg } from '../utils';
import paper from 'paper';

export const poseParts = ['leftAnkle', 'leftKnee', 'leftHip', 'leftWrist', 'leftElbow', 'leftShoulder',
  'rightAnkle', 'rightKnee', 'rightHip', 'rightWrist', 'rightElbow', 'rightShoulder',
  'leftEar', 'rightEar'];

export const faceParts = [
  'topMid', 'rightTop0', 'rightTop1', 'leftTop0', 'leftTop1',
  'rightJaw0', 'rightJaw1', 'rightJaw2', 'rightJaw3', 'rightJaw4', 'rightJaw5', 'rightJaw6', 'rightJaw7', 'jawMid',   // 0 - 8
  'leftJaw7', 'leftJaw6', 'leftJaw5', 'leftJaw4', 'leftJaw3', 'leftJaw2', 'leftJaw1', 'leftJaw0', // 9 - 16
  'rightBrow0', 'rightBrow1', 'rightBrow2', 'rightBrow3', 'rightBrow4', // 17 - 21
  'leftBrow4', 'leftBrow3', 'leftBrow2', 'leftBrow1', 'leftBrow0', // 22 - 26
  'nose0', 'nose1', 'nose2', 'nose3', // 27 - 30
  'rightNose0', 'rightNose1', 'nose4', 'leftNose1', 'leftNose0', // 31 - 35
  'rightEye0', 'rightEye1', 'rightEye2', 'rightEye3', 'rightEye4', 'rightEye5', // 36 - 41
  'leftEye3', 'leftEye2', 'leftEye1', 'leftEye0', 'leftEye5', 'leftEye4', // 42 - 47
  'rightMouthCorner', 'rightUpperLipTop0', 'rightUpperLipTop1', 'upperLipTopMid', 'leftUpperLipTop1', 'leftUpperLipTop0', 'leftMouthCorner', // 48 - 54
  'leftLowerLipBottom0', 'leftLowerLipBottom1', 'lowerLipBottomMid', 'rightLowerLipBottom1', 'rightLowerLipBottom0', // 55 - 59
  'rightMiddleLip', 'rightUpperLipBottom1', 'upperLipBottomMid', 'leftUpperLipBottom1', 'leftMiddleLip', // 60 - 64
  'leftLowerLipTop0', 'lowerLipTopMid', 'rightLowerLipTop0' // 65 - 67
];

export const allParts = faceParts.concat(poseParts);

// Mapping between face part names and their vertex indices in TF face mesh.
export const facePartToIndex = {
  'topMid': 10,
  'rightTop0': 67,
  'rightTop1': 54,
  'leftTop0': 297,
  'leftTop1': 284,
  'rightJaw0': 21,
  'rightJaw1': 162,
  'rightJaw2': 127,
  'rightJaw3': 234,
  'rightJaw4': 132,
  'rightJaw5': 172,
  'rightJaw6': 150,
  'rightJaw7': 176,
  'jawMid': 152,   // 0 - 8
  'leftJaw7': 400,
  'leftJaw6': 379,
  'leftJaw5': 397,
  'leftJaw4': 361,
  'leftJaw3': 454,
  'leftJaw2': 356,
  'leftJaw1': 389,
  'leftJaw0': 251, // 9 - 16
  'rightBrow0': 46,
  'rightBrow1': 53,
  'rightBrow2': 52,
  'rightBrow3': 65,
  'rightBrow4': 55, // 17 - 21
  'leftBrow4': 285,
  'leftBrow3': 295,
  'leftBrow2': 282,
  'leftBrow1': 283,
  'leftBrow0': 276, // 22 - 26
  'nose0': 6,
  'nose1': 197,
  'nose2': 195,
  'nose3': 5, // 27 - 30
  'rightNose0': 48,
  'rightNose1': 220,
  'nose4': 4,
  'leftNose1': 440,
  'leftNose0': 278, // 31 - 35
  'rightEye0': 33,
  'rightEye1': 160,
  'rightEye2': 158,
  'rightEye3': 133,
  'rightEye4': 153,
  'rightEye5': 144, // 36 - 41
  'leftEye3': 362,
  'leftEye2': 385,
  'leftEye1': 387,
  'leftEye0': 263,
  'leftEye5': 373,
  'leftEye4': 380, // 42 - 47
  'rightMouthCorner': 61,
  'rightUpperLipTop0': 40,
  'rightUpperLipTop1': 37,
  'upperLipTopMid': 0,
  'leftUpperLipTop1': 267,
  'leftUpperLipTop0': 270,
  'leftMouthCorner': 291, // 48 - 54
  'leftLowerLipBottom0': 321,
  'leftLowerLipBottom1': 314,
  'lowerLipBottomMid': 17,
  'rightLowerLipBottom1': 84,
  'rightLowerLipBottom0': 91, // 55 - 59
  'rightMiddleLip': 78,
  'rightUpperLipBottom1': 81,
  'upperLipBottomMid': 13,
  'leftUpperLipBottom1': 311,
  'leftMiddleLip': 308, // 60 - 64
  'leftLowerLipTop0': 402,
  'lowerLipTopMid': 14,
  'rightLowerLipTop0': 178 // 65 - 67
};

function getKP(group, partName) {
  const shape = svg.findFirstItemWithPrefix(group, partName);
  return {
    position: shape.bounds.center,
    name: partName
  };
}

export function mapKpToParts(group) {
  const body = {
    face: {},
    pose: {}
  };

  // Exclude the right & left ear
  poseParts.filter(x => !['leftEar', 'rightEar'].includes(x))
    .forEach(part => body.pose[part] = getKP(group, part));
  faceParts.forEach(part => body.face[part] = getKP(group, part));
  return body;
}

export function getPartFromPose(pose, name) {
  if (!pose || !pose.keypoints) {
    return null;
  }
  let part = pose.keypoints.find(kp => kp.part === name);
  return {
    position: new paper.Point(part.position.x, part.position.y),
    score: part.score
  };
}

export function getKeypointFromFaceFrame(face, i) {
  return new paper.Point(face.positions[i * 2], face.positions[i * 2 + 1]);
}
