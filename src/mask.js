import { AnimeRenderer } from './renderer/anime';

class Mask {
  constructor(stream) {
    this.stream = stream;
    // Destination element
    this.canvas = document.createElement('canvas');
    // Source element
    this.video = document.createElement('video');
  }

  get output() {
    return new Promise(async res => {
      const video = await this._initVideoStream(this.stream);

      // TODO: allow user to choose filter / renderer
      this.renderer = new AnimeRenderer(video, this.canvas);

      console.debug('Initializing renderer');
      await this.renderer.init();

      console.debug('Rendering');
      this.update();
      res(this.canvas.captureStream());
    });
  }

  update(t) {
    // Update the renderer
    this.renderer.render(t);
    requestAnimationFrame(this.update.bind(this));
  }

  _initVideoStream(stream) {
    console.debug('Setting up stream');
    return new Promise((res) => {
      this.video.srcObject = stream;
      this.video.autoplay = true;
      this.video.onloadeddata = () => res(this.video);
    });
  }
}

export { Mask };
