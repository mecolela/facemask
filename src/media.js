import { Mask } from './mask.js';

function initFaceMask() {
  if (typeof MediaDevices === 'undefined') {
    console.debug('FACE MASK NOT INSTALLED.');
    return;
  }

  const enumerateDevicesFn = MediaDevices.prototype.enumerateDevices;
  const getUserMediaFn = MediaDevices.prototype.getUserMedia;

  MediaDevices.prototype.enumerateDevices = async function () {
    const devices = await enumerateDevicesFn.call(navigator.mediaDevices);
    if (!devices.find(x => x.deviceId === 'facemask')) {
      // Hijack the device enumerator and add our own
      devices.push({
        deviceId: 'facemask',
        groupId: 'mask',
        kind: 'videoinput',
        label: 'Face Mask'
      });
    }

    console.debug('FACE MASK INSTALLED.');
    return devices;
  };

  MediaDevices.prototype.getUserMedia = async function () {
    const args = arguments;

    if (args.length && args[0].video && args[0].video.deviceId) {
      if (args[0].video.deviceId === 'facemask' || args[0].video.deviceId.exact === 'facemask') {
        // TODO: allow user to switch input source
        //  let the caller get the camera for us.
        const constraints = {
          video: {
            facingMode: args[0].facingMode,
            advanced: args[0].video.advanced,
            width: args[0].video.width,
            height: args[0].video.height
          },
          audio: false
        };

        const stream = await getUserMediaFn.call(navigator.mediaDevices, constraints);

        // Prepare our own stream
        const mask = new Mask(stream);
        return await mask.output;
      }
    }

    return await getUserMediaFn.call(navigator.mediaDevices, ...arguments);
  };
}

export { initFaceMask };
